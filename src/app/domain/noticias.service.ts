import { Injectable } from "@angular/core";

@Injectable()
export class NoticiasService {

    private noticas: Array<string> = [];

    agregar(s: string) {
        this.noticas.push(s);
    }

    buscar(): Array<string> {
        return this.noticas;
    }

}
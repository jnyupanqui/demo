import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { FuncionalidadRoutingModule } from "./funcionalidad-routing.module";
import { ClimaComponent } from "./clima/clima.component";
import { NoticiaComponent } from "./noticia/noticia.component";
import { FuncionalidadComponent } from "./funcionalidad.component";
  
@NgModule({
    imports: [
        FuncionalidadRoutingModule,
        NativeScriptCommonModule
    ],
    declarations: [
        FuncionalidadComponent,
        ClimaComponent,
        NoticiaComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class FuncionalidadModule { }

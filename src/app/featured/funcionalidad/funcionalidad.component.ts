import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "Funcionalidad",
    templateUrl: "./funcionalidad.component.html"
})
export class FuncionalidadComponent {

    message: string;

    constructor(private routerExtensions: RouterExtensions) {

        if (app.android) {
            this.message = "Hola desde ANDROID";
        }
        else {
            this.message = "Hola desde IOS";
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}

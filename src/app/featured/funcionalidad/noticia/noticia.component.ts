import { Component, OnInit } from "@angular/core";
import { NoticiasService } from "~/app/domain/noticias.service";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

@Component({
    selector: "Noticia",
    templateUrl: "./noticia.component.html"
}
)
export class NoticiaComponent implements OnInit {
    constructor(public noticiasService: NoticiasService) {}

    ngOnInit(): void {
        this.noticiasService.agregar("Noticia#1");
        this.noticiasService.agregar("Noticia#2");
        this.noticiasService.agregar("Noticia#3");
    }
    
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
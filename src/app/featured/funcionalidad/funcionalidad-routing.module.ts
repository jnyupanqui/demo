import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NoticiaComponent } from "./noticia/noticia.component";
import { ClimaComponent } from "./clima/clima.component";
import { FuncionalidadComponent } from "./funcionalidad.component";


const routes: Routes = [
    { path: "", component: FuncionalidadComponent },
    { path: "noticia", component: NoticiaComponent },
    { path: "clima", component: ClimaComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})

export class FuncionalidadRoutingModule { }